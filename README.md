# (Dynopt Assignment 2)Optimizing walking controller #

Vidya Narayanan, Jim Bern

### Code ###

* c-cmaes
* walk (provided base code with modifications)
* useful (provided base code)

### Compiling and running ###

* Requires ODE installation
* Running make should build the executables (modified to work with OS X)
* walk/cmaes_p0.par has the initial parameter configuration and cmaes options
* walk/cmaes_signals.par adds cmaes iteration limit options that can be edited
* Run ./learn to optimize controller starting with the initial configuration 
* Optimized parameters are saved as best.txt
* Run ./simulate parameters.txt perturbation_value to run the simulation with
the optimized parameters
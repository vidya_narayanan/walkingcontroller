/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "main.h"
#include "main2.h"

#include "cmaes_interface.h"
/*****************************************************************************/
/*****************************************************************************/

extern SIM sim;
int  do_perturbation = 1;
/*****************************************************************************/
/*****************************************************************************/
// Example of optimizing a value
/*****************************************************************************/
/*****************************************************************************/


double gaussrand(double mean, double stddev)
{
    static double V1, V2, S;
    static int phase = 0;
    double X;

    // how much standard deviation do we want?
    
    
    if(phase == 0) {
        do {
            double U1 = (double)rand() / RAND_MAX;
            double U2 = (double)rand() / RAND_MAX;

            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
        } while(S >= 1 || S == 0);

        X = V1 * sqrt(-2 * log(S) / S);
    } else
        X = V2 * sqrt(-2 * log(S) / S);

    phase = 1 - phase;

    return mean + X * stddev;
}


double run_sim( SIM *sim )
{
  int i;

  for( i = 0; sim->time < sim->duration; i++ )
    {
      controller( sim );
      save_data( sim );
      if ( sim->status == CRASHED )
          	break; 
      integrate_one_time_step( sim );
    }

  // write_the_mrdplot_file( sim );
  return get_score( sim );
}

double fun(double const *x, int N){

    // set sim with all the initial values
    // x should have 21 parameters
    {
        reinit_sim(&sim);
        
        sim.swing_time = x[0];
        sim.thrust1 = x[1];
        sim.swing_hip_target = x[2];
        sim.swing_hv1 = x[3];
        sim.swing_ha1 = x[4];
        sim.swing_knee1 = x[5];
        sim.swing_kv1 = x[6];
        sim.swing_ka1 = x[7];
        sim.swing_knee_target = x[8];
        sim.swing_kv2 = x[9];
        sim.swing_ka2 = x[10];
        sim.stance_hip_target = x[11];
        sim.stance_hv1 = x[12];
        sim.stance_ha1 = x[13];
        sim.pitch_d = x[14];
        sim.stance_kv1 = x[15];
        sim.stance_ka1 = x[16];
        sim.stance_knee_target = x[17];
        sim.stance_kv2 = x[18];
        sim.stance_ka2 = x[19];
        sim.stance_ankle_torque = x[20];


        // apply random perturbation from a gaussian (white noise)
        if(do_perturbation){
            sim.torso_perturbation =  gaussrand(100., 1.); 
        }
    }


    return run_sim(&sim);
}
/*****************************************************************************/

int main( int argc, char **argv )
{

  
  int i;
  PARAMETER *params;
  int n_parameters;
  double score, new_score;

  init_default_parameters( &sim );
  sim.rand_scale = 0;
  sim.controller_print = 1;

  /* Parame:ter file argument? */
  /*if ( argc > 1 )
    {
      params = read_parameter_file( argv[1] );
      n_parameters = process_parameters( params, &sim, 1 );
      if ( n_parameters > MAX_N_PARAMETERS )
	{
	  fprintf( stderr, "Too many parameters %d > %d\n",
		   n_parameters, MAX_N_PARAMETERS );
	  exit( -1 );
	}
    }*/

  init_sim( &sim );
  init_data( &sim );

  sim.controller_print = 0;


  cmaes_t evo;
  double *arFunvals, *const*pop, *xmean, *xbest;

  arFunvals = cmaes_init(&evo, 0, NULL, NULL, 0, 0, "cmaes_p0.par");
  printf("%s\n",cmaes_SayHello(&evo));
  // using default signals, not sure if this is relevant and needs fiddling
  cmaes_ReadSignals(&evo, "cmaes_signals.par");
  
  if((int)cmaes_Get(&evo, "dim" ) != 21){
    printf("Problem dimension does not match!\n");
    exit(1);
  }

  while(!cmaes_TestForTermination(&evo)){
    pop = cmaes_SamplePopulation(&evo);
    for(i = 0; i < cmaes_Get(&evo, "lambda"); ++i){
        arFunvals[i] = fun(pop[i], (int) cmaes_Get(&evo, "dim"));
    }
    cmaes_UpdateDistribution(&evo, arFunvals);
    cmaes_ReadSignals(&evo, "cmaes_signals.par");
    fflush(stdout);
  }

  printf("Stop:\n%s\n", cmaes_TestForTermination(&evo));
  cmaes_WriteToFile(&evo, "all", "learning.dat");
  xmean = cmaes_GetNew(&evo, "xmean"); //or xbestever
  xbest = cmaes_GetNew(&evo, "xbestever"); //or xbestever
  
  FILE *meanFile;
  FILE *bestFile;
  meanFile = fopen("mean.txt", "w");
  bestFile = fopen("best.txt", "w");
  char* list[21]={ 
        "swing_time ",
        "thrust1 ",
        "swing_hip_target ", 
        "swing_hv1 ",
        "swing_ha1 ",
        "swing_knee1 ",
        "swing_kv1 ",
        "swing_ka1 ",
        "swing_knee_target ",
        "swing_kv2 ",
        "swing_ka2 ",
        "stance_hip_target ",
        "stance_hv1 ",
        "stance_ha1 ",
        "pitch_d ",
        "stance_kv1 ",
        "stance_ka1 ",
        "stance_knee_target ",
        "stance_kv2 ",
        "stance_ka2 ",
        "stance_ankle_torque "
    };

  for(int i = 0; i < 21; i++){
    fprintf(meanFile, "%s %f opt end \n",list[i], xmean[i]);
    fprintf(bestFile, "%s %f opt end \n", list[i], xbest[i]);
  }
  fclose(meanFile);
  fclose(bestFile);
  
  cmaes_exit(&evo);

  //figure out if xfinal is the parameter or the value
  //do something with it, i.e save it and run simulate

  free(xmean);
  free(xbest);
  /*
  score = run_sim( &sim );
  // show how to run simulation multiple times.
  
  
  for ( i = 0; i < 10; i++ )
    {
      reinit_sim( &sim );
      sim.thrust1 -= 0.001;
      new_score = run_sim( &sim );
      if ( new_score > score ){
	      fprintf(stderr, "Stopped simulation after %d runs.",i);
          break;
      }
      score = new_score;
    }
    */
}

/*****************************************************************************/
